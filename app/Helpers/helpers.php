<?php

function googleImageSearch(string $term, int $start = 0): array
{
    $url = 'https://www.googleapis.com/customsearch/v1?';
    $params = [
        'key='.env('CSA_KEY'),
        'cx='.env('CSE_ID'),
        'q='.urlencode($term),
        'start='.$start,
        'searchType=image',
        'imgSize=xxlarge'
    ];

    $query = $url.implode('&', $params);
    $res = json_decode(file_get_contents($query));

    $images = [];
    foreach ($res->items as $item) {
        $images[] = $item->link;
    }

    return $images;
}
