<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CustomSearchController extends Controller
{
    public function index(Request $request)
    {
        $params = $request->all();
        $start = isset($params['start']) ? intval($params['start']) : 0;
        return googleImageSearch($params['term'], $start);
    }
}
