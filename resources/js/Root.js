import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";
import Application from "./components/Application";

function Root() {
    return <Application />;
}

export default Root;
ReactDOM.render(<Root />, document.getElementById("root"));
