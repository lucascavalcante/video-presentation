import React from "react";
import styled from "styled-components";

function Button(props) {
    return (
        <button
            className={props.className}
            disabled={props.btn ? false : true}
            onClick={props.type}
        >
            {props.label}
        </button>
    );
}

const StyledButton = styled(Button)`
    background-color: lightskyblue;
    margin-top: 30px;
    padding: 10px 20px;
    border: none;
    border-radius: 3px;
`;

export default StyledButton;
