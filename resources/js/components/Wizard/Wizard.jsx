import React, { useState } from "react";
import Button from "./Button";
import Path from "./Path";

const getButtonsState = (indx, length) => {
    if (indx > 0 && indx < length - 1) {
        return {
            showPreviousBtn: true,
            showNextBtn: true
        };
    } else if (indx === 0) {
        return {
            showPreviousBtn: false,
            showNextBtn: true
        };
    } else {
        return {
            showPreviousBtn: true,
            showNextBtn: false
        };
    }
};

const getNavStyles = (indx, length) => {
    let styles = [];
    for (let i = 0; i < length; i++) {
        if (i < indx) {
            styles.push("done");
        } else if (i === indx) {
            styles.push("doing");
        } else {
            styles.push("todo");
        }
    }
    return styles;
};

export default function Wizard(props) {
    let showNavigation = true;
    if (props.showNavigation && props.showNavigation)
        showNavigation = props.showNavigation;

    const [stylesState, setStyles] = useState(
        getNavStyles(0, props.steps.length)
    );
    const [compState, setComp] = useState(0);
    const [buttonsState, setButtons] = useState(
        getButtonsState(0, props.steps.length)
    );

    const setStepState = indx => {
        setStyles(getNavStyles(indx, props.steps.length));
        setComp(indx < props.steps.length ? indx : compState);
        setButtons(getButtonsState(indx, props.steps.length));
    };

    const next = () => setStepState(compState + 1);
    const previous = () =>
        setStepState(compState > 0 ? compState - 1 : compState);
    const handleKeyDown = evt =>
        evt.which === 13 ? next(props.steps.length) : {};

    const handleOnClick = evt => {
        if (
            evt.currentTarget.value === props.steps.length - 1 &&
            compState === props.steps.length - 1
        ) {
            setStepState(props.steps.length);
        } else {
            setStepState(evt.currentTarget.value);
        }
    };

    return (
        <div onKeyDown={handleKeyDown} className="wizard">
            <Path
                steps={props.steps}
                onClick={handleOnClick}
                styles={stylesState}
            />
            <div className="row justify-content-around">
                {showNavigation && (
                    <Button
                        btn={buttonsState.showPreviousBtn}
                        type={previous}
                        label="Previous"
                    />
                )}
                {showNavigation && (
                    <Button
                        btn={buttonsState.showNextBtn}
                        type={next}
                        label="Next"
                    />
                )}
            </div>
            <div>{props.steps[compState].component}</div>
        </div>
    );
}
