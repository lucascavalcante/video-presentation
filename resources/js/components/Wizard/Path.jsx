import React, { useState } from "react";
import styled from "styled-components";
import { CheckCircle } from "@styled-icons/boxicons-solid/CheckCircle";

function Path(props) {
    return props.steps.map((s, i) => (
        <li
            className={props.styles[i]}
            onClick={props.onClick}
            key={i}
            value={i}
        >
            {props.styles[i] === "todo" && <Todo size="18" />}
            {props.styles[i] === "doing" && <Doing size="18" />}
            {props.styles[i] === "done" && <Done size="18" />}
            <span>{s.name}</span>
        </li>
    ));
}

const Todo = styled(CheckCircle)`
    color: silver;
`;

const Doing = styled(CheckCircle)`
    color: gray;
`;

const Done = styled(CheckCircle)`
    color: green;
`;

const StyledPath = styled(Path)`
    margin: 0;
    padding-bottom: 2.2rem;
    list-style-type: none;
`;

export default StyledPath;
