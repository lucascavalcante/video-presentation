import React, { useState } from "react";
import FormSearch from "./FormSearch";
import ImageList from "./ImageList";

function Step1(props) {
    const [imagesFromSearch, setImagesFromSearch] = useState([]);

    const handleSubmit = async event => {
        event.preventDefault();
        props.setIsLoading(true);

        const classSelectedImages = document.getElementsByClassName(
            "selected-image"
        );

        [].slice.call(classSelectedImages).map(image => {
            image.classList.remove("selected-image");
        });

        const search = event.target.querySelector(
            'input[name="google-search"]'
        );
        const results = await axios.get("/google-images-search", {
            params: {
                term: search.value
            }
        });
        props.setIsLoading(false);
        setImagesFromSearch(results.data);
    };

    return (
        <>
            <FormSearch handleSubmit={handleSubmit} />
            <ImageList
                imagesFromSearch={imagesFromSearch}
                selectedImages={props.selectedImages}
                setSelectedImages={props.setSelectedImages}
            />
        </>
    );
}

export default Step1;
