import React from "react";
import styled from "styled-components";
import { SearchAlt2 } from "@styled-icons/boxicons-regular/SearchAlt2";

function ButtonSearch(props) {
    return (
        <button type="submit" className={props.className}>
            <IconSearch size="36" />
        </button>
    );
}

const IconSearch = styled(SearchAlt2)`
    color: white;
`;

const StyledButtonSearch = styled(ButtonSearch)`
    background: tomato;
    padding: 10px;
    font-size: 24pt;
    border: 1px solid tomato;
    border-radius: 0 5px 5px 0;
    font-family: Montserrat;
`;

export default StyledButtonSearch;
