import React from "react";
import styled from "styled-components";

function InputSearch(props) {
    return (
        <input className={props.className} type="search" name="google-search" />
    );
}

const StyledInputSearch = styled(InputSearch)`
    padding: 10px;
    font-size: 24pt;
    border-radius: 5px 0 0 5px;
    border: 1px solid #ccc;
    font-family: Montserrat;
    width: 508px;
`;

export default StyledInputSearch;
