import React from "react";
import styled from "styled-components";

function Image(props) {
    const checkImage = event => {
        event.target.src = "https://via.placeholder.com/150";
    };

    return (
        <img
            key={props.key}
            src={props.src}
            className={props.className}
            onError={checkImage}
            onClick={props.onClick}
        />
    );
}

const StyledImage = styled(Image)`
    width: ${props => props.size}px;
    height: ${props => props.size}px;
    object-fit: cover;
    margin: 5px;
    border-radius: 5px;
`;

export default StyledImage;
