import React, { useState, useEffect } from "react";
import styled from "styled-components";
import InputSearch from "./InputSearch";
import ButtonSearch from "./ButtonSearch";

function FormSearch(props) {
    return (
        <form className={props.className} onSubmit={props.handleSubmit}>
            <InputSearch />
            <ButtonSearch />
        </form>
    );
}

const StyledFormSearch = styled(FormSearch)`
    margin: 20px 0;
`;

export default StyledFormSearch;
