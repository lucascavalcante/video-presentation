import React from "react";
import Image from "./Image";

function ImageList(props) {
    const handleClick = event => {
        const target = event.target;
        const arraySelectedImages = [...props.selectedImages];
        if (!arraySelectedImages.includes(target.src)) {
            arraySelectedImages.push(target.src);
            target.classList.add("selected-image");
        } else {
            arraySelectedImages.splice(
                arraySelectedImages.indexOf(target.src),
                1
            );
            target.classList.remove("selected-image");
        }

        props.setSelectedImages(arraySelectedImages);
    };

    return (
        <div className="d-flex flex-wrap justify-content-around">
            {props.imagesFromSearch.map((image, index) => (
                <Image
                    key={index}
                    src={image}
                    onClick={handleClick}
                    size="150"
                />
            ))}
        </div>
    );
}

export default ImageList;
