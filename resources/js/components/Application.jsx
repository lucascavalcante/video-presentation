import React, { useState } from "react";
import Wizard from "./Wizard/Wizard";
import Step1 from "./Step1/Step1";
import Step2 from "./Step2/Step2";
import Step3 from "./Step3/Step3";
import Image from "./Step1/Image";
import Spinner from "./common/Spinner";

function Application() {
    const [selectedImages, setSelectedImages] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const steps = [
        {
            name: "Selecione as Images",
            component: (
                <Step1
                    selectedImages={selectedImages}
                    setSelectedImages={setSelectedImages}
                    setIsLoading={setIsLoading}
                />
            )
        },
        { name: "Adicione o Áudio", component: <Step2 /> },
        { name: "Adicione as Legendas", component: <Step3 /> }
    ];
    return (
        <>
            {isLoading && (
                <Spinner>
                    <div className="lds-ripple">
                        <div></div>
                        <div></div>
                    </div>
                </Spinner>
            )}
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-12 text-center">
                        <Wizard steps={steps} />
                    </div>
                </div>
            </div>
            <div className="col-md-12 text-center">
                {selectedImages.map((image, i) => {
                    return <Image key={i} src={image} size="50" />;
                })}
            </div>
        </>
    );
}

export default Application;
